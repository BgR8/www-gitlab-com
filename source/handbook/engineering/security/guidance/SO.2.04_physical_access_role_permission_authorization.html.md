---
layout: markdown_page
title: "SO.2.04 - Physical Access Role Permission Authorization Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SO.2.04 - Physical Access Role Permission Authorization

## Control Statement

Initial permission definitions, and changes to permissions, associated with physical access roles are approved by authorized personnel.

## Context

This control refers to all roles used for granting physical access to GitLab facilities.

## Scope

This control is not applicable since GitLab has no facilities.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Physical Access Role Permission Authorization control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/895).

### Policy Reference

## Framework Mapping

* ISO
  * A.11.1.5
  * A.11.1.6
* SOC2 CC
  * CC6.4
